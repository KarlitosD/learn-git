# Git
## **¿Que es git?**
Hoy en día, Git es, con diferencia, el sistema de control de versiones moderno más utilizado del mundo. Git es un proyecto de código abierto maduro y con un mantenimiento activo que desarrolló originalmente Linus Torvalds, el famoso creador del kernel del sistema operativo Linux, en 2005. 
---

## **Comandos**
### Configuracion global del nombre de usuario
```
git config --global user.name "!username"
```
### Configuracion global del correo de usuario
```
git config --global user.email "!email@email.com"
```
### Inicializar el repositorio
```
git init
```
### Agregar un archivo al staging(area de cambios antes de registrarlo en el repositorio)
```
git add !nombreArchivo
```
### Una altenativa es agregar todos los archivos que hallan sufrido un cambio
```
git add .
```
### Quitar un archivos del staging
```
git rm !nombreArchivo
```
### Quitar todos los archivos del staging
```
git rm .
```
### Para ver en que estado esta nuestro repositorio y el staging usamos
```
git status
```
### Para registrar de una vez los cambios en el repositorio hacemos un commit al cual hay que darle un titulo y una descripcion
```
git commit
```
### Para hacer un commit solo con un titulo
```
git commit -m "!Titulo"
```
### Usamos el parametro -am para hacer un add y un commit con un titulo en uno solo comando
```
git commit -am "!Titulo"
```
### Para ver los cambios hechos en los archivos comparados con los que hay en el repositorio antes de agregarlos al staging
```
git diff
```
### Historial de commits 
```
git log
```
### Historial de cambios remusido
```
git log --oneline
```
### Ver cambios hechos en determinado commit con el id del git log
```
git diff !idCommit
```
### Agregar repositorio remoto con el cual vamos a trabajar
```
git remote add origin !url
```
### Si trabajamos con muchos remotos hay que darle un nombre a casa uno
```
git remote add !nameRemote !url
```
### Ver con los remotos que estamos trabajando
```
git remote -v
```
### Traer los archivos del remoto
```
git pull origin master
```
### Para conservar los cambios de tu repositorio
```
git pull --rebase origin master
```
### Para mandar los cambios al repositorio remoto
```
git push origin master
```
### Revertir los cambios a los del ultimo commit de un archivo
```
git checkout -- archiveName!
```
### Ver la rama en la que estamos
```
git branch
```
### Para ver todas las ramas, inclusos las remotas
```
git branch --all
```
### Crear una rama nueva
```
git branch !newBranchName
```
### Cambiar de rama
```
git branch !branchName
```
### Crear una rama y movernos en un solo comando
```
git checkout -b !newBranchName
```
### Eliminar una rama
```
git branch -d !branchName
```
### Fusionar ramas (toBrenchName seria que repositorio queremos traer los cambios)
```
git merge !toBrenchName
```
### Copiar repositorio remoto (cuando copias un repositorio remoto ya lo establaces como uno y ya no hace falta el "git remote add")
```
git clone !url
```